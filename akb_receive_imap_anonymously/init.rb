Redmine::Plugin.register :akb_receive_imap_anonymously do
  name 'Receive IMAP emails anonymously (patch)'
  author 'Arne-Kolja Bachstein'
  description 'This plugin enhances the task redmine:email:receive_imap with better anonymous mail handling.'
  version '0.0.1'
  url 'https://bitbucket.org/arnekolja/receive-imap-emails-anonymously-redmine-plugin'
  author_url 'info@akbwebservices.de'
end

Dir[File.dirname(__FILE__) + "/lib/patches/*.rb"].each { |file| require(file) }
