module AkbReceiveImapAnonymously
  module MailHandlerPatch

    def self.included(base) # :nodoc:
      base.send(:include, InstanceMethods)

      base.class_eval do
        alias_method_chain :receive_issue, :anonymous_patch
      end
    end

    module InstanceMethods
      private

      def receive_issue_with_anonymous_patch
        puts "Using patched version of receive_issue."

        issue = receive_issue_without_anonymous_patch

        if issue
          if user.type == "AnonymousUser"
            puts "User is #{user}. Adding email to the issue description."
            issue.description += "\n\n---\n\n#{t('.email_from', :default => 'From')}: #{email.from.to_a.first.to_s.strip}"

            issue.save!
          else
            puts "User is #{user}. User is not anonymous."
          end
        end
      end
    end

  end
end

ActionDispatch::Callbacks.to_prepare do
  MailHandler.send(:include, AkbReceiveImapAnonymously::MailHandlerPatch)
end
